/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 * this file is responsible for bootstrapping the application
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import 'reflect-metadata';
import { AppModule } from './app/app.module';
import * as cors from 'cors';
// import * as socketio from 'socket.io';
// import * as http from 'http';//

async function bootstrap() {
  // An async function is defined with the keyword "async" before the function definition, and it always returns a promise, whether it is resolved or rejected
  const app = await NestFactory.create(AppModule);
  const port = process.env['PORT'] || 3333;
  app.use(cors());
  app.enableCors();

  // const server = http.createServer(app.getHttpServer());
  // const io = socketio(server);
  // io.origins('*:*');

  await app.listen(port); // The function will not continue the execution after await statement until the promise is resolved.
  Logger.log(`🚀 Application is running on: http://localhost:${port}/`);
}

bootstrap();
