import { Test, TestingModule } from '@nestjs/testing';

import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(() => {
    appService = new AppService();
    appController = new AppController(appService);
  });

  describe('getData', () => {
    it('should return "Welcome to backend!"', async () => {
      const result = { message: 'Welcome to backend!' };
      jest.spyOn(appService, 'getData').mockImplementation(() => result);
      expect(await appController.getData()).toBe(result);
    });
  });
});
