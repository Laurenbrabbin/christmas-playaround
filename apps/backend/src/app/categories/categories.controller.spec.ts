import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CategoriesEntity } from './categories.entity';

const mockCategories = [new CategoriesEntity(), new CategoriesEntity(), new CategoriesEntity()];

describe('CategoriesController', () => {
  let categoriesController: CategoriesController;
  let categoriesService: CategoriesService;

  const mockedRepo = {
    find: jest.fn((query) => Promise.resolve(mockCategories)),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CategoriesService,
        {
          provide: getRepositoryToken(CategoriesEntity),
          useValue: mockedRepo,
        },
      ],
    }).compile();
    categoriesService = await module.get(CategoriesService);
    categoriesController = new CategoriesController(categoriesService);
  });
  afterEach(() => jest.clearAllMocks());

  describe('getData', () => {
    it('should return Categories', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'find');
      const result = await categoriesService.getCategories();
      expect(result).toEqual(mockCategories);
      expect(findSpy).toHaveBeenCalledTimes(1);
      expect(await categoriesController.getData()).toBe(result);
    });
  });
});
