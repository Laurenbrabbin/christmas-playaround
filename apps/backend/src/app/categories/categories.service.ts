import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoriesEntity } from './categories.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoriesEntity) // injecting repository into service - is an entity’s access layer used to make queries (insert, delete, save, find, etc.)
    private repo: Repository<CategoriesEntity>
  ) {}

  async getCategories(): Promise<CategoriesEntity[]> {
    return this.repo.find();
  }
}
