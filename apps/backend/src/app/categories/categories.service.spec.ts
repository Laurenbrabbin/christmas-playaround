import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CategoriesService } from './categories.service';
import { CategoriesEntity } from './categories.entity';

const mockCategories = [new CategoriesEntity(), new CategoriesEntity(), new CategoriesEntity()];

describe('CategoriesService', () => {
  let service: CategoriesService;

  const mockedRepo = {
    find: jest.fn((query) => Promise.resolve(mockCategories)),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CategoriesService,
        {
          provide: getRepositoryToken(CategoriesEntity),
          useValue: mockedRepo,
        },
      ],
    }).compile();
    service = await module.get(CategoriesService);
  });
  afterEach(() => jest.clearAllMocks());

  describe('getCategories', () => {
    it('should return Categories', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'find');

      const result = await service.getCategories();
      expect(result).toEqual(mockCategories);

      expect(findSpy).toHaveBeenCalledTimes(1);
    });
  });
});
