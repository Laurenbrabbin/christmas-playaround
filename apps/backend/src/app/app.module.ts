import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { CategoriesModule } from './categories/categories.module';
import { SuggestionsModule } from './suggestions/suggestions.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsEntity } from './products/products.entity';
import { CategoriesEntity } from './categories/categories.entity';
import { SuggestionsEntity } from './suggestions/suggestions.entity';
import { CorsMiddleware } from '@nest-middlewares/cors';
import { RequestMethod } from '@nestjs/common';

@Module({
  imports: [
    SuggestionsModule,
    ProductsModule,
    CategoriesModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'christmas.db',
      entities: [ProductsEntity, CategoriesEntity, SuggestionsEntity],
      synchronize: true, //for database
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(CorsMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
