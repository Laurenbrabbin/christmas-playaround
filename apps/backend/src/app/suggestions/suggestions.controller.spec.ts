import { SuggestionsController } from './suggestions.controller';
import { SuggestionsService } from './suggestions.service';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SuggestionsEntity } from './suggestions.entity';

const mockSuggestions = [new SuggestionsEntity(), new SuggestionsEntity(), new SuggestionsEntity()];
const mockSuggestion = new SuggestionsEntity();

describe('SuggestionsController', () => {
  let suggestionsController: SuggestionsController;
  let suggestionsService: SuggestionsService;

  const mockedRepo = {
    find: jest.fn((query) => Promise.resolve(mockSuggestions)),
    save: jest.fn((suggestion) => Promise.resolve(suggestion)),
    delete: jest.fn((id) => Promise.resolve()),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SuggestionsService,
        {
          provide: getRepositoryToken(SuggestionsEntity),
          useValue: mockedRepo,
        },
      ],
    }).compile();
    suggestionsService = await module.get(SuggestionsService);
    suggestionsController = new SuggestionsController(suggestionsService);
  });
  afterEach(() => jest.clearAllMocks());

  describe('getData', () => {
    it('should return Suggestions', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'find');
      const result = await suggestionsService.getSuggestions();
      expect(result).toEqual(mockSuggestions);
      expect(findSpy).toHaveBeenCalledTimes(1);
      expect(await suggestionsController.getData()).toBe(result);
    });
  });
  describe('create', () => {
    it('should be called once', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'save');
      const serviceResult = await suggestionsService.create(mockSuggestion);
      expect(serviceResult).toEqual(mockSuggestion);
      expect(findSpy).toHaveBeenCalledTimes(1);
    });
  });
});
