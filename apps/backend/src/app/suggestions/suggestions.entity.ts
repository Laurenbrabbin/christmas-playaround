import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class SuggestionsEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  product: string;
}
