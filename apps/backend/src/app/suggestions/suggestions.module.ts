import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SuggestionsController } from './suggestions.controller';
import { SuggestionsService } from './suggestions.service';
import { SuggestionsEntity } from './suggestions.entity';
import { SuggestionsGateway } from './suggestions.gateway';

@Module({
  imports: [TypeOrmModule.forFeature([SuggestionsEntity])],
  controllers: [SuggestionsController],
  providers: [SuggestionsService, SuggestionsGateway],
})
export class SuggestionsModule {}
