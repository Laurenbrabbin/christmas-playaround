import { Body, Controller, Get, Post, Delete, Param } from '@nestjs/common';
import { SuggestionsService } from './suggestions.service';
import { SuggestionsEntity } from './suggestions.entity';
import { SuggestionDto } from './suggestion.dto';

@Controller('suggestions')
export class SuggestionsController {
  constructor(private readonly suggestionsService: SuggestionsService) {}

  @Get()
  async getData(): Promise<SuggestionsEntity[]> {
    return this.suggestionsService.getSuggestions();
  }

  @Post()
  async create(@Body() suggestion: SuggestionDto) {
    // return this.suggestionsService.create(suggestion);
    console.log('controller');
    return suggestion;
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return this.suggestionsService.deleteSuggestion(+id);
  }
}
