import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SuggestionsService } from './suggestions.service';
import { SuggestionsEntity } from './suggestions.entity';

const mockSuggestions = [new SuggestionsEntity(), new SuggestionsEntity(), new SuggestionsEntity()];
const mockSuggestion = new SuggestionsEntity();

describe('SuggestionsService', () => {
  let service: SuggestionsService;

  const mockedRepo = {
    find: jest.fn((query) => Promise.resolve(mockSuggestions)),
    save: jest.fn((suggestion) => Promise.resolve(suggestion)),
    delete: jest.fn((id) => Promise.resolve()),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SuggestionsService,
        {
          provide: getRepositoryToken(SuggestionsEntity),
          useValue: mockedRepo,
        },
      ],
    }).compile();
    service = await module.get(SuggestionsService);
  });
  afterEach(() => jest.clearAllMocks());

  describe('getSuggestions', () => {
    it('should return Suggestions', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'find');

      const result = await service.getSuggestions();
      expect(result).toEqual(mockSuggestions);

      expect(findSpy).toHaveBeenCalledTimes(1);
    });
  });
  describe('create', () => {
    it('should be called once', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'save');

      const result = await service.create(mockSuggestion);
      expect(result).toEqual(mockSuggestion);

      expect(findSpy).toHaveBeenCalledTimes(1);
    });
  });
  describe('deleteSuggestion', () => {
    it('should be called once', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'delete');

      const result = await service.deleteSuggestion(mockSuggestion.id);
      expect(result).toEqual(undefined);

      expect(findSpy).toHaveBeenCalledTimes(1);
    });
  });
});
