import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SuggestionsEntity } from './suggestions.entity';
import { SuggestionDto } from './suggestion.dto';
import { SuggestionsGateway } from './suggestions.gateway';

@Injectable()
export class SuggestionsService {
  constructor(
    @InjectRepository(SuggestionsEntity)
    private repo: Repository<SuggestionsEntity>
  ) {}

  async getSuggestions(): Promise<SuggestionsEntity[]> {
    let suggestions = await this.repo.find();
    return suggestions;
  }

  async create(suggestionDto: SuggestionDto) {
    const suggestion = new SuggestionsEntity();
    suggestion.product = suggestionDto.product;
    await this.repo.save(suggestion);
    return suggestion;
  }

  async deleteSuggestion(id: number) {
    await this.repo.delete({ id });
  }
}
