import { WebSocketGateway, WebSocketServer, OnGatewayInit, SubscribeMessage } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Logger } from '@nestjs/common';
import { SuggestionsService } from './suggestions.service';
import { SuggestionsEntity } from './suggestions.entity';

@WebSocketGateway({ cors: true, origins: 'http://localhost:4200', namespace: 'suggestions' })
export class SuggestionsGateway implements OnGatewayInit {
  @WebSocketServer() server: Server;

  constructor(private readonly suggestionsService: SuggestionsService) {}

  private logger = new Logger('SuggestionGateway');

  afterInit(server: Server) {
    console.log(`Socket.io initialized`);
  }

  handleConnection(client: Socket) {
    console.log('websocket connected');
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('createSuggestion')
  async createSuggestion(client: Socket, suggestion: SuggestionsEntity) {
    const createdSuggestion = await this.suggestionsService.create(suggestion);
    this.server.emit('suggestionsUpdated', { createdSuggestion });
  }
}
