import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ProductsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  productName: string;

  @Column()
  productCode: string;

  @Column()
  description: string;

  @Column()
  price: number;

  @Column()
  categoryId: number;

  @Column()
  quantityInStock: number;
}
