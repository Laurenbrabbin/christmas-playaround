import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ProductsEntity } from './products.entity';

const mockProducts = [new ProductsEntity(), new ProductsEntity(), new ProductsEntity()];

describe('ProductsController', () => {
  let productsController: ProductsController;
  let productsService: ProductsService;

  const mockedRepo = {
    find: jest.fn((query) => Promise.resolve(mockProducts)),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductsService,
        {
          provide: getRepositoryToken(ProductsEntity),
          useValue: mockedRepo,
        },
      ],
    }).compile();
    productsService = await module.get(ProductsService);
    productsController = new ProductsController(productsService);
  });
  afterEach(() => jest.clearAllMocks());

  describe('getData', () => {
    it('should return Products', async () => {
      const findSpy = jest.spyOn(mockedRepo, 'find');
      const result = await productsService.getProducts();
      expect(result).toEqual(mockProducts);
      expect(findSpy).toHaveBeenCalledTimes(1);
      expect(await productsController.getData()).toBe(result);
    });
  });
});
