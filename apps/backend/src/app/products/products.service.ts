import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductsEntity } from './products.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(ProductsEntity)
    private repo: Repository<ProductsEntity>
  ) {}

  async getProducts(): Promise<ProductsEntity[]> {
    return this.repo.find();
  }
}
