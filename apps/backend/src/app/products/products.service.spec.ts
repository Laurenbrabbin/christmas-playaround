import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ProductsService } from './products.service';
import { ProductsEntity } from './products.entity';

// create fake entiites, just for testing. Here they are empty,
// but they can be more complex, depending on the testing cases.
const mockProducts = [new ProductsEntity(), new ProductsEntity(), new ProductsEntity()];

describe('ProductsService', () => {
  let service: ProductsService;

  const mockedRepo = {
    // mock the repo `find`
    find: jest.fn((query) => Promise.resolve(mockProducts)),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductsService,
        // Mock the repository using the `getRepositoryToken` from @nestjs/typeorm
        {
          provide: getRepositoryToken(ProductsEntity),
          useValue: mockedRepo,
        },
      ],
    }).compile();
    // get the service from the testing module.
    service = await module.get(ProductsService);
  });

  // reset call counts and called with arguments after each spec
  afterEach(() => jest.clearAllMocks());

  describe('getProducts', () => {
    it('should return Products', async () => {
      // spy for the repository findOneOrFail method
      const findSpy = jest.spyOn(mockedRepo, 'find');

      // When we call a service function the following things happen:
      // - the real service function is called, so we can test its code
      // - the mocked repository method is called

      const result = await service.getProducts();
      // check the result against the expected results
      expect(result).toEqual(mockProducts);

      // Ensure that the spies are called once with the appropriate arguments
      expect(findSpy).toHaveBeenCalledTimes(1);
    });
  });
});
