import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { colourReducer } from '../../../../libs/frontend/state/colour.reducer';
import { NewProductFormComponent } from '../../../../libs/frontend/suggestions/form/new-product-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from '../../../../libs/frontend/table/table.component';
import { SuggestionTableComponent } from '../../../../libs/frontend/suggestions/table/suggestion-table.component';
import { NavbarComponent } from '../../../../libs/frontend/navbar/navbar.component';
import { ProductModule } from '../../../../libs/frontend/products/product.module';
import { WelcomeModule } from '../../../../libs/frontend/home/welcome.module';
import { DarkModeModule } from '../../../../libs/frontend/darkmode/darkmode.module';

@NgModule({
  imports: [
    BrowserModule,
    StoreModule.forRoot({ green: colourReducer }),
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ProductModule,
    WelcomeModule,
    DarkModeModule,
  ],
  declarations: [AppComponent, NewProductFormComponent, TableComponent, SuggestionTableComponent, NavbarComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
