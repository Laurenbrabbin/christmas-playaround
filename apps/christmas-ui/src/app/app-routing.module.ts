import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WelcomeComponent } from '../../../../libs/frontend/home/welcome.component';
import { NewProductFormComponent } from '../../../../libs/frontend/suggestions/form/new-product-form.component';
import { SuggestionTableComponent } from '../../../../libs/frontend/suggestions/table/suggestion-table.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      // { path: 'welcome', component: WelcomeComponent },
      { path: 'suggestions', component: NewProductFormComponent },
      { path: 'table', component: SuggestionTableComponent },
      {
        path: 'welcome',
        loadChildren: () => import('../../../../libs/frontend/home/welcome.module').then((m) => m.WelcomeModule),
      },
      {
        path: 'products',
        loadChildren: () => import('../../../../libs/frontend/products/product.module').then((m) => m.ProductModule),
      },

      { path: '', redirectTo: 'welcome', pathMatch: 'full' },
    ]),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
