describe('homepage test', () => {
  it('homepage button redirects to product page', () => {
    cy.visit('http://localhost:4200');
    cy.get('button#products').click();
    cy.location('pathname').should('eq', '/products');
  });
  it('homepage button redirects to suggestion page', () => {
    cy.visit('http://localhost:4200');
    cy.get('button#suggestions').click();
    cy.location('pathname').should('eq', '/suggestions');
  });
});
