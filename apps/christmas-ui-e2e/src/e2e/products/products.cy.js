const products = require('../../cypress/fixtures/products.json');
describe('Product table test', () => {
  it('has 8 food products', () => {
    cy.visit('http://localhost:4200/products');
    cy.intercept('GET', '/products/*', products);
    cy.get('table').find('tr').should('have.length', 9);
  });
  it('has 8 food products', () => {
    cy.intercept('http://localhost:3000/products');
    cy.visit('http://localhost:4200/products');
    cy.get('table').find('tr').should('have.length', 9);
  });
  it('has 3 sides', () => {
    cy.intercept('http://localhost:3000/products');
    cy.intercept('http://localhost:3000/categories');
    cy.visit('http://localhost:4200/products');
    cy.get('select#category-select').select(3);
    cy.get('table').find('tr').should('have.length', 3);
  });
});
