describe('testing suggestions', () => {
  it('displays user input', () => {
    cy.visit('http://localhost:4200/suggestions');
    cy.get('input').type('big turkey');
    cy.contains('Submit').click();
    cy.get('table').should('contain', 'big turkey');
  });
  it('removes user input', () => {
    cy.visit('http://localhost:4200/suggestions');
    cy.get('input').type('sprouts');
    cy.contains('Submit').click();
    cy.get('table').should('contain', 'sprouts');
    cy.get('button#remove').last().click();
    cy.get('table').not('contain', 'sprouts');
  });
});
