## Running the app

mock-servers: json-server products.json
frontend: nx run christmas-ui:serve:development
backend: nx serve backend

## Tests

nx test backend
npm libs
ng run christmas-ui:test
ng run christmas-ui-e2e:e2e
ng run backend:test
