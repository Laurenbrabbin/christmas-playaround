import { HttpMethod, createHttpFactory, SpectatorHttp } from '@ngneat/spectator';
import { SuggestionService } from './suggestion.service';
import { HttpTestingController } from '@angular/common/http/testing';

describe('SuggestionService', () => {
  let spectator: SpectatorHttp<SuggestionService>;
  let service: SuggestionService;
  let controller: HttpTestingController;

  const createHttp = createHttpFactory(SuggestionService);

  beforeEach(() => {
    (spectator = createHttp()), (service = spectator.service), (controller = spectator.inject(HttpTestingController));
  });

  afterEach(() => {
    controller.verify();
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  xit('tests POST request', () => {
    spectator.service.addSuggestion('suggestion');
    const req = spectator.expectOne('//localhost:3333/suggestions', HttpMethod.POST);
    expect(req.request.body).toEqual('suggestion');
  });
});
