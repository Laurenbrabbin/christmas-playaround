import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs';
import { DataService } from '../../data/data.service';
import { io } from 'socket.io-client';

@Injectable({
  providedIn: 'root',
})
export class SuggestionService {
  private socket: any;

  constructor(private http: HttpClient, private dataService: DataService) {
    this.socket = io('http://localhost:3333/suggestions');
  }
  // nestJS: string = '//localhost:3333/suggestions';
  // jsonServer: string = '//localhost:3000/suggestions';

  addSuggestion(product: string) {
    this.socket.emit('createSuggestion', product);
    this.socket.on('suggestionsUpdated', (res: any) => {
      console.log(res);
      this.dataService.notifyOther({ refresh: true });
    });
    // return this.http.post('//localhost:3333/suggestions', product);
  }

  removeSuggestion(id: number) {
    return this.http.delete(`//localhost:3333/suggestions/${id}`);
  }

  suggestionCreated() {
    this.socket.on('suggestionsUpdated', (res: any) => {
      console.log('way back');
      console.log(res);
      this.dataService.notifyOther({ refresh: true });
    });
  }

  suggestions$ = this.http
    .get<any[]>('//localhost:3333/suggestions')
    .pipe(tap((data) => console.log('Suggestions: ', JSON.stringify(data))));
}
