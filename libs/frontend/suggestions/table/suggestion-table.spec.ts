import { Spectator, createComponentFactory, byText, toExist } from '@ngneat/spectator';
import { MockProvider, MockInstance } from 'ng-mocks';
import { Store } from '@ngrx/store';
import { of, EMPTY } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SuggestionTableComponent } from './suggestion-table.component';
import { SuggestionService } from '../service/suggestion.service';
import { DataService } from '../../data/data.service';
import { TableComponent } from '../../table/table.component';

describe('SuggestionTableComponent', () => {
  let spectator: Spectator<SuggestionTableComponent>;
  let component: SuggestionTableComponent;
  const mockSuggestionsObservable = require('../../mock-observables/suggestion-observable');

  const createComponent = createComponentFactory({
    component: SuggestionTableComponent,
    providers: [
      MockProvider(Store),
      MockProvider(DataService, {
        notifyObservable$: EMPTY,
      }),
      MockProvider(SuggestionService, {
        suggestions$: EMPTY,
      }),
    ],
    imports: [HttpClientTestingModule],
    declarations: [TableComponent],
  });

  beforeEach(() => {
    spectator = createComponent({
      props: {
        suggestions$: mockSuggestionsObservable,
      },
    });
    component = spectator.component;
  });

  it('should create', () => {
    expect(SuggestionTableComponent).toBeTruthy();
  });

  it('should display the product table', () => {
    expect(spectator.query('.suggestion-table')).toBeVisible();
  });

  it('should display products', () => {
    expect(spectator.query(byText('sprouts'))).toExist();
    expect(spectator.query(byText('crackers'))).toExist();
  });
});
