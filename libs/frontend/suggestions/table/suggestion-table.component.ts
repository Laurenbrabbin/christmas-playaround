import { Component, OnInit } from '@angular/core';
import { SuggestionService } from '../service/suggestion.service';
import { catchError, EMPTY } from 'rxjs';
import { DataService } from '../../data/data.service';
import { Store } from '@ngrx/store';

@Component({
  selector: 'suggestion-table',
  templateUrl: './suggestion-table.component.html',
  styleUrls: ['./suggestion-table.component.css'],
})
export class SuggestionTableComponent implements OnInit {
  errorMessage = '';
  greenText$ = this.store.select('green');

  constructor(
    private suggestionService: SuggestionService,
    private dataService: DataService,
    private store: Store<{ green: string }>
  ) {}

  suggestions$ = this.suggestionService.suggestions$.pipe(
    catchError((err) => {
      this.errorMessage = err;
      return EMPTY;
    })
  );

  ngOnInit(): void {
    this.dataService.notifyObservable$.subscribe((res) => {
      if (res.refresh) {
        this.suggestions$ = this.suggestionService.suggestions$.pipe(
          catchError((err) => {
            this.errorMessage = err;
            return EMPTY;
          })
        );
      }
    });
  }

  onSubmit(id: number) {
    this.suggestionService.removeSuggestion(id).subscribe((res) => {
      this.dataService.notifyOther({ refresh: true });
    });
  }
}
