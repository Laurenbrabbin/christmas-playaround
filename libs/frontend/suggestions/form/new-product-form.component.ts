import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { SuggestionService } from '../service/suggestion.service';
import { DataService } from '../../data/data.service';

@Component({
  selector: 'new-product-form',
  templateUrl: './new-product-form.component.html',
  styleUrls: ['./new-product-form.component.css'],
})
export class NewProductFormComponent {
  productForm: FormGroup;
  formBuilder: FormBuilder;
  greenText$ = this.store.select('green');

  constructor(
    formBuilder: FormBuilder,
    private store: Store<{ green: string }>,
    private suggestionService: SuggestionService,
    private dataService: DataService
  ) {
    this.formBuilder = formBuilder;
    this.productForm = this.formBuilder.group({
      product: '',
    });
  }

  onSubmit() {
    this.suggestionService.addSuggestion(this.productForm.value);
    this.productForm.reset();
  }
}
