import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { ProductCategory } from './product-category';
import { FormBuilder, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  categories$ = this.http.get<ProductCategory[]>('//localhost:3000/categories').pipe(
    tap((data) => console.log('Categories: ', JSON.stringify(data))),
    catchError(this.handleError)
  );

  constructor(private http: HttpClient) {}

  private handleError(err: HttpErrorResponse): Observable<never> {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.message}`;
    }
    console.error(err);
    return throwError(() => errorMessage);
  }
}
