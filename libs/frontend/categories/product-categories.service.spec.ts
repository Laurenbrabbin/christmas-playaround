import { HttpMethod, createHttpFactory, SpectatorHttp } from '@ngneat/spectator';
import { CategoriesService } from './product-category.service';

describe('CategoriesService', () => {
  let spectator: SpectatorHttp<CategoriesService>;
  let service: CategoriesService;

  const createHttp = createHttpFactory(CategoriesService);

  beforeEach(() => {
    (spectator = createHttp()), (service = spectator.service);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
  it('tests CategoryService', () => {
    spectator.service.categories$.subscribe();
    spectator.expectOne('//localhost:3000/categories', HttpMethod.GET);
  });
});
