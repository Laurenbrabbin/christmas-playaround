import { Input, Output, EventEmitter, Component } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'font-sizer',
  templateUrl: './font-size.component.html',
  styleUrls: ['./font-size.component.css'],
})
export class SizerComponent {
  @Input() size!: number | string;
  @Output() sizeChange = new EventEmitter<number>();

  dec() {
    this.resize(-1);
  }
  inc() {
    this.resize(+1);
  }

  resize(delta: number) {
    this.size = Math.min(40, Math.max(8, +this.size + delta));
    this.sizeChange.emit(this.size);
  }
}
