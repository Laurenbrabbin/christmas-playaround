import { NgModule } from '@angular/core';
import { SizerComponent } from './font-size.component';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [StoreModule],
  exports: [SizerComponent],
  declarations: [SizerComponent],
  providers: [],
})
export class FontSizeModule {}
