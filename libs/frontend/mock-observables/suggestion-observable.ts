import { of } from 'rxjs';

const mockSuggestionsObservable = of([
  {
    product: 'cranberry sauce',
    id: 1,
  },
  {
    product: 'sprouts',
    id: 2,
  },
  {
    product: 'crackers',
    id: 3,
  },
]);

module.exports = mockSuggestionsObservable;
