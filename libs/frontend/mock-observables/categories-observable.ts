import { of } from 'rxjs';

const mockCategoriesObservable = of([
  { id: 1, name: 'Meat' },
  { id: 2, name: 'Vegetables' },
]);

module.exports = mockCategoriesObservable;
