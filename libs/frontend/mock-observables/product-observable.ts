import { of } from 'rxjs';

const mockProductsObservable = of([
  {
    id: 1,
    productName: 'Turkey',
    productCode: '0011',
    description: 'Turkey that serves 8 people',
    price: 40.0,
    categoryId: 1,
    quantityInStock: 15,
  },
  {
    id: 2,
    productName: 'Roast Potatos',
    productCode: '0012',
    description: 'Roast Potatos for 6 people',
    price: 4.0,
    categoryId: 2,
    quantityInStock: 25,
  },
]);

module.exports = mockProductsObservable;
