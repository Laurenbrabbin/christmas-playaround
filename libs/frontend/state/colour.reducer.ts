import { createReducer, on } from '@ngrx/store'
import { changeColour } from './colour.actions' 

export const initialState = true

export const colourReducer = createReducer(
    initialState,
    on(changeColour, (state) => !state )
)