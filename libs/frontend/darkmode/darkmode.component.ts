import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'darkmode',
  templateUrl: './darkmode.component.html',
  styleUrls: ['./darkmode.component.css'],
})
export class DarkmodeComponent {
  @Output() modeChange = new EventEmitter<boolean>();

  lightMode: boolean = true;

  constructor() {}

  changeMode() {
    this.lightMode = !this.lightMode;
    this.modeChange.emit(this.lightMode);
  }
}
