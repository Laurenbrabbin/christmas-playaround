import { NgModule } from '@angular/core';
import { DarkmodeComponent } from './darkmode.component';
import { CommonModule } from '@angular/common';
@NgModule({
  imports: [CommonModule],
  exports: [DarkmodeComponent],
  declarations: [DarkmodeComponent],
  providers: [],
})
export class DarkModeModule {}
