import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './product-list/product-list.component';
import { StoreModule } from '@ngrx/store';
import { FontSizeModule } from '../font-size/font-size.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FontSizeModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProductListComponent,
      },
    ]),
    StoreModule,
  ],
  declarations: [ProductListComponent],
})
export class ProductModule {}
