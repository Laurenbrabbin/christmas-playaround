import { Spectator, createComponentFactory, byText } from '@ngneat/spectator';
import { MockProvider } from 'ng-mocks';
import { ProductListComponent } from './product-list.component';
import { Store } from '@ngrx/store';
import { ProductService } from '../service/product.service';
import { CategoriesService } from '../../categories/product-category.service';
import { EMPTY } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProductListComponent', () => {
  let spectator: Spectator<ProductListComponent>;
  let component: ProductListComponent;
  const mockCategoriesObservable = require('../../mock-observables/categories-observable');
  const mockProductsObservable = require('../../mock-observables/product-observable');

  const createComponent = createComponentFactory({
    component: ProductListComponent,
    providers: [
      MockProvider(Store),
      MockProvider(CategoriesService, {
        categories$: EMPTY,
      }),
      MockProvider(ProductService, {
        products$: EMPTY,
      }),
    ],
    imports: [HttpClientTestingModule],
  });

  beforeEach(() => {
    spectator = createComponent({
      props: {
        products$: mockProductsObservable,
        categories$: mockCategoriesObservable,
      },
    });
    component = spectator.component;
  });

  it('should create', () => {
    expect(ProductListComponent).toBeTruthy();
  });

  it('should display the product table', () => {
    expect(spectator.query('table')).toBeVisible();
  });

  it('should display the dropdown menu', () => {
    expect(spectator.query('.dropdown')).toBeVisible();
  });

  it('should display products', () => {
    expect(spectator.query(byText('Turkey'))).toExist();
    expect(spectator.query(byText('Roast Potatos'))).toExist();
    expect(spectator.query(byText('Random words which should not exist'))).not.toExist();
  });

  xit('should set the correct option on standard select', () => {
    const select = spectator.query('.dropdown') as HTMLSelectElement;
    spectator.selectOption(select, ['1']);
    expect(select).toHaveSelectedOptions(['1']);
  });
});
