import { ChangeDetectionStrategy, Component } from '@angular/core';
import { catchError, combineLatest, EMPTY, Observable, Subject, map, startWith } from 'rxjs';
import { ProductService } from '../service/product.service';
import { Store } from '@ngrx/store';
import { CategoriesService } from '../../categories/product-category.service';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductListComponent {
  private categorySelectedSubject = new Subject<number>();
  categorySelectedAction$ = this.categorySelectedSubject.asObservable();

  pageTitle = 'Product List';
  errorMessage = '';
  categories = [];
  greenText$ = this.store.select('green');

  products$ = combineLatest([
    this.productService.products$, // data stream
    this.categorySelectedAction$.pipe(startWith(0)), // action stream
  ]).pipe(
    map(([products, selectedCategoryID]) =>
      products.filter((product) => (selectedCategoryID ? product.categoryId === selectedCategoryID : true))
    ),
    catchError((err) => {
      this.errorMessage = err;
      return EMPTY;
    })
  );

  categories$ = this.categoriesService.categories$.pipe(
    catchError((err) => {
      this.errorMessage = err;
      return EMPTY;
    })
  );

  constructor(
    private productService: ProductService,
    private categoriesService: CategoriesService,
    private store: Store<{ green: string }>
  ) {}

  onSelected(categoryId: string): void {
    this.categorySelectedSubject.next(+categoryId);
  }
}
