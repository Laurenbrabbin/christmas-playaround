import { HttpMethod, createHttpFactory, SpectatorHttp } from '@ngneat/spectator';
import { ProductService } from './product.service';

describe('ProductService', () => {
  let spectator: SpectatorHttp<ProductService>;
  let service: ProductService;

  const createHttp = createHttpFactory(ProductService);

  beforeEach(() => {
    (spectator = createHttp()), (service = spectator.service);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('tests ProductService', () => {
    spectator.service.products$.subscribe();
    spectator.expectOne('//localhost:3000/products', HttpMethod.GET);
  });
});
