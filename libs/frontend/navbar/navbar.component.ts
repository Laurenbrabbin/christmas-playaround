import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { changeColour } from '../../frontend/state/colour.actions';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  pageTitle = 'Christmas Food';
  greenText$ = this.store.select('green');
  lightMode: boolean = true;

  constructor(private store: Store<{ green: string }>) {}

  changeColour() {
    this.store.dispatch(changeColour());
  }

  changeMode() {
    this.lightMode = !this.lightMode;
    console.log(this.lightMode);
  }
}
