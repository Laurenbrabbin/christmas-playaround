import { NgModule } from '@angular/core';
import { FontSizeModule } from '../font-size/font-size.module';
import { WelcomeComponent } from './welcome.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FontSizeModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: WelcomeComponent,
      },
    ]),
  ],
  exports: [WelcomeComponent],
  declarations: [WelcomeComponent],
})
export class WelcomeModule {}
