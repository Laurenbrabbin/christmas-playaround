import { Spectator, createComponentFactory, mockProvider } from '@ngneat/spectator';
import { WelcomeComponent } from './welcome.component';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { SizerComponent } from '../font-size/font-size.component';

describe('WelcomeComponent', () => {
  let spectator: Spectator<WelcomeComponent>;
  let component: WelcomeComponent;
  let routerSpy = { navigate: jasmine.createSpy('navigate') };

  const createComponent = createComponentFactory({
    component: WelcomeComponent,
    providers: [mockProvider(Store), { provide: Router, useValue: routerSpy }, SizerComponent],
    declarations: [SizerComponent],
  });

  beforeEach(() => {
    (spectator = createComponent()), (component = spectator.component);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('redirects to product page when button is clicked', () => {
    spectator.click('button');
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/products']);
  });
});
