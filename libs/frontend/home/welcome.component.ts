import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

@Component({
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
})
export class WelcomeComponent {
  greenText$ = this.store.select('green');
  fontSizePx = 16;

  constructor(private router: Router, private store: Store<{ green: string }>) {}

  public pageTitle = 'Welcome';

  viewProducts() {
    this.router.navigate(['/products']);
  }

  makeSuggestion() {
    this.router.navigate(['/suggestions']);
  }
}
