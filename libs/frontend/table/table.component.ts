import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'table-template',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent {
  greenText$ = this.store.select('green');

  constructor(private store: Store<{ green: string }>) {}
}
