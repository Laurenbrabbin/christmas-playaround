import { Spectator, createComponentFactory, mockProvider } from '@ngneat/spectator';
import { TableComponent } from './table.component';
import { Store } from '@ngrx/store';

describe('WelcomeComponent', () => {
  let spectator: Spectator<TableComponent>;
  let component: TableComponent;

  const createComponent = createComponentFactory({
    component: TableComponent,
    providers: [mockProvider(Store)],
  });

  beforeEach(() => {
    (spectator = createComponent()), (component = spectator.component);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
