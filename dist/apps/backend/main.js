/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	// The require scope
/******/ 	var __webpack_require__ = {};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

;// CONCATENATED MODULE: external "@nestjs/common"
const common_namespaceObject = require("@nestjs/common");
;// CONCATENATED MODULE: external "@nestjs/core"
const core_namespaceObject = require("@nestjs/core");
;// CONCATENATED MODULE: external "reflect-metadata"
const external_reflect_metadata_namespaceObject = require("reflect-metadata");
;// CONCATENATED MODULE: external "tslib"
const external_tslib_namespaceObject = require("tslib");
;// CONCATENATED MODULE: ./src/app/app.service.ts


let AppService = class AppService {
    getData() {
        return { message: 'Welcome to backend!' };
    }
};
AppService = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Injectable)()
], AppService);


;// CONCATENATED MODULE: ./src/app/app.controller.ts
var _a;



let AppController = class AppController {
    constructor(appService) {
        this.appService = appService;
    }
    getData() {
        return this.appService.getData();
    }
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Get)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Function),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", []),
    (0,external_tslib_namespaceObject.__metadata)("design:returntype", void 0)
], AppController.prototype, "getData", null);
AppController = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Controller)(),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (_a = typeof AppService !== "undefined" && AppService) === "function" ? _a : Object])
], AppController);


;// CONCATENATED MODULE: external "@nestjs/typeorm"
const typeorm_namespaceObject = require("@nestjs/typeorm");
;// CONCATENATED MODULE: external "typeorm"
const external_typeorm_namespaceObject = require("typeorm");
;// CONCATENATED MODULE: ./src/app/products/products.entity.ts


let ProductsEntity = class ProductsEntity {
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.PrimaryGeneratedColumn)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Number)
], ProductsEntity.prototype, "id", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", String)
], ProductsEntity.prototype, "productName", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", String)
], ProductsEntity.prototype, "productCode", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", String)
], ProductsEntity.prototype, "description", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Number)
], ProductsEntity.prototype, "price", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Number)
], ProductsEntity.prototype, "categoryId", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Number)
], ProductsEntity.prototype, "quantityInStock", void 0);
ProductsEntity = (0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Entity)()
], ProductsEntity);


;// CONCATENATED MODULE: ./src/app/products/products.service.ts
var products_service_a;





let ProductsService = class ProductsService {
    constructor(repo) {
        this.repo = repo;
    }
    async getProducts() {
        return this.repo.find();
    }
};
ProductsService = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Injectable)(),
    (0,external_tslib_namespaceObject.__param)(0, (0,typeorm_namespaceObject.InjectRepository)(ProductsEntity)),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (products_service_a = typeof external_typeorm_namespaceObject.Repository !== "undefined" && external_typeorm_namespaceObject.Repository) === "function" ? products_service_a : Object])
], ProductsService);


;// CONCATENATED MODULE: ./src/app/products/products.controller.ts
var products_controller_a;



let ProductsController = class ProductsController {
    constructor(productsService) {
        this.productsService = productsService;
    }
    getData() {
        return this.productsService.getProducts();
    }
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Get)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Function),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", []),
    (0,external_tslib_namespaceObject.__metadata)("design:returntype", void 0)
], ProductsController.prototype, "getData", null);
ProductsController = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Controller)('products'),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (products_controller_a = typeof ProductsService !== "undefined" && ProductsService) === "function" ? products_controller_a : Object])
], ProductsController);


;// CONCATENATED MODULE: ./src/app/products/products.module.ts






let ProductsModule = class ProductsModule {
};
ProductsModule = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Module)({
        imports: [typeorm_namespaceObject.TypeOrmModule.forFeature([ProductsEntity])],
        controllers: [ProductsController],
        providers: [ProductsService],
    })
], ProductsModule);


;// CONCATENATED MODULE: ./src/app/categories/categories.entity.ts


let CategoriesEntity = class CategoriesEntity {
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.PrimaryGeneratedColumn)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Number)
], CategoriesEntity.prototype, "id", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", String)
], CategoriesEntity.prototype, "name", void 0);
CategoriesEntity = (0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Entity)()
], CategoriesEntity);


;// CONCATENATED MODULE: ./src/app/categories/categories.service.ts
var categories_service_a;





let CategoriesService = class CategoriesService {
    constructor(repo) {
        this.repo = repo;
    }
    async getCategories() {
        return this.repo.find();
    }
};
CategoriesService = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Injectable)(),
    (0,external_tslib_namespaceObject.__param)(0, (0,typeorm_namespaceObject.InjectRepository)(CategoriesEntity)),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (categories_service_a = typeof external_typeorm_namespaceObject.Repository !== "undefined" && external_typeorm_namespaceObject.Repository) === "function" ? categories_service_a : Object])
], CategoriesService);


;// CONCATENATED MODULE: ./src/app/categories/categories.controller.ts
var categories_controller_a, _b;



let CategoriesController = class CategoriesController {
    constructor(categoriesService) {
        this.categoriesService = categoriesService;
    }
    async getData() {
        return this.categoriesService.getCategories();
    }
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Get)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Function),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", []),
    (0,external_tslib_namespaceObject.__metadata)("design:returntype", typeof (_b = typeof Promise !== "undefined" && Promise) === "function" ? _b : Object)
], CategoriesController.prototype, "getData", null);
CategoriesController = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Controller)('categories'),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (categories_controller_a = typeof CategoriesService !== "undefined" && CategoriesService) === "function" ? categories_controller_a : Object])
], CategoriesController);


;// CONCATENATED MODULE: ./src/app/categories/categories.module.ts






let CategoriesModule = class CategoriesModule {
};
CategoriesModule = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Module)({
        imports: [typeorm_namespaceObject.TypeOrmModule.forFeature([CategoriesEntity])],
        controllers: [CategoriesController],
        providers: [CategoriesService],
    })
], CategoriesModule);


;// CONCATENATED MODULE: ./src/app/suggestions/suggestions.entity.ts


let SuggestionsEntity = class SuggestionsEntity {
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.PrimaryGeneratedColumn)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Number)
], SuggestionsEntity.prototype, "id", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Column)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", String)
], SuggestionsEntity.prototype, "product", void 0);
SuggestionsEntity = (0,external_tslib_namespaceObject.__decorate)([
    (0,external_typeorm_namespaceObject.Entity)()
], SuggestionsEntity);


;// CONCATENATED MODULE: ./src/app/suggestions/suggestions.service.ts
var suggestions_service_a;





let SuggestionsService = class SuggestionsService {
    constructor(repo) {
        this.repo = repo;
    }
    async getSuggestions() {
        let suggestions = await this.repo.find();
        return suggestions;
    }
    async create(suggestionDto) {
        const suggestion = new SuggestionsEntity();
        suggestion.product = suggestionDto.product;
        await this.repo.save(suggestion);
        return suggestion;
    }
    async deleteSuggestion(id) {
        await this.repo.delete({ id });
    }
};
SuggestionsService = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Injectable)(),
    (0,external_tslib_namespaceObject.__param)(0, (0,typeorm_namespaceObject.InjectRepository)(SuggestionsEntity)),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (suggestions_service_a = typeof external_typeorm_namespaceObject.Repository !== "undefined" && external_typeorm_namespaceObject.Repository) === "function" ? suggestions_service_a : Object])
], SuggestionsService);


;// CONCATENATED MODULE: ./src/app/suggestions/suggestion.dto.ts
class SuggestionDto {
}

;// CONCATENATED MODULE: ./src/app/suggestions/suggestions.controller.ts
var suggestions_controller_a, suggestions_controller_b, _c;




let SuggestionsController = class SuggestionsController {
    constructor(suggestionsService) {
        this.suggestionsService = suggestionsService;
    }
    async getData() {
        return this.suggestionsService.getSuggestions();
    }
    async create(suggestion) {
        // return this.suggestionsService.create(suggestion);
        console.log('controller');
        return suggestion;
    }
    async remove(id) {
        return this.suggestionsService.deleteSuggestion(+id);
    }
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Get)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Function),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", []),
    (0,external_tslib_namespaceObject.__metadata)("design:returntype", typeof (suggestions_controller_b = typeof Promise !== "undefined" && Promise) === "function" ? suggestions_controller_b : Object)
], SuggestionsController.prototype, "getData", null);
(0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Post)(),
    (0,external_tslib_namespaceObject.__param)(0, (0,common_namespaceObject.Body)()),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Function),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (_c = typeof SuggestionDto !== "undefined" && SuggestionDto) === "function" ? _c : Object]),
    (0,external_tslib_namespaceObject.__metadata)("design:returntype", Promise)
], SuggestionsController.prototype, "create", null);
(0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Delete)(':id'),
    (0,external_tslib_namespaceObject.__param)(0, (0,common_namespaceObject.Param)('id')),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Function),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [String]),
    (0,external_tslib_namespaceObject.__metadata)("design:returntype", Promise)
], SuggestionsController.prototype, "remove", null);
SuggestionsController = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Controller)('suggestions'),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (suggestions_controller_a = typeof SuggestionsService !== "undefined" && SuggestionsService) === "function" ? suggestions_controller_a : Object])
], SuggestionsController);


;// CONCATENATED MODULE: external "@nestjs/websockets"
const websockets_namespaceObject = require("@nestjs/websockets");
;// CONCATENATED MODULE: external "socket.io"
const external_socket_io_namespaceObject = require("socket.io");
;// CONCATENATED MODULE: ./src/app/suggestions/suggestions.gateway.ts
var suggestions_gateway_a, suggestions_gateway_b, suggestions_gateway_c, _d;






let SuggestionsGateway = class SuggestionsGateway {
    constructor(suggestionsService) {
        this.suggestionsService = suggestionsService;
        this.logger = new common_namespaceObject.Logger('SuggestionGateway');
    }
    afterInit(server) {
        console.log(`Socket.io initialized`);
    }
    handleConnection(client) {
        console.log('websocket connected');
    }
    handleDisconnect(client) {
        this.logger.log(`Client disconnected: ${client.id}`);
    }
    async createSuggestion(client, suggestion) {
        const createdSuggestion = await this.suggestionsService.create(suggestion);
        this.server.emit('suggestionsUpdated', { createdSuggestion });
    }
};
(0,external_tslib_namespaceObject.__decorate)([
    (0,websockets_namespaceObject.WebSocketServer)(),
    (0,external_tslib_namespaceObject.__metadata)("design:type", typeof (suggestions_gateway_b = typeof external_socket_io_namespaceObject.Server !== "undefined" && external_socket_io_namespaceObject.Server) === "function" ? suggestions_gateway_b : Object)
], SuggestionsGateway.prototype, "server", void 0);
(0,external_tslib_namespaceObject.__decorate)([
    (0,websockets_namespaceObject.SubscribeMessage)('createSuggestion'),
    (0,external_tslib_namespaceObject.__metadata)("design:type", Function),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (suggestions_gateway_c = typeof external_socket_io_namespaceObject.Socket !== "undefined" && external_socket_io_namespaceObject.Socket) === "function" ? suggestions_gateway_c : Object, typeof (_d = typeof SuggestionsEntity !== "undefined" && SuggestionsEntity) === "function" ? _d : Object]),
    (0,external_tslib_namespaceObject.__metadata)("design:returntype", Promise)
], SuggestionsGateway.prototype, "createSuggestion", null);
SuggestionsGateway = (0,external_tslib_namespaceObject.__decorate)([
    (0,websockets_namespaceObject.WebSocketGateway)({ cors: true, origins: 'http://localhost:4200', namespace: 'suggestions' }),
    (0,external_tslib_namespaceObject.__metadata)("design:paramtypes", [typeof (suggestions_gateway_a = typeof SuggestionsService !== "undefined" && SuggestionsService) === "function" ? suggestions_gateway_a : Object])
], SuggestionsGateway);


;// CONCATENATED MODULE: ./src/app/suggestions/suggestions.module.ts







let SuggestionsModule = class SuggestionsModule {
};
SuggestionsModule = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Module)({
        imports: [typeorm_namespaceObject.TypeOrmModule.forFeature([SuggestionsEntity])],
        controllers: [SuggestionsController],
        providers: [SuggestionsService, SuggestionsGateway],
    })
], SuggestionsModule);


;// CONCATENATED MODULE: external "@nest-middlewares/cors"
const cors_namespaceObject = require("@nest-middlewares/cors");
;// CONCATENATED MODULE: ./src/app/app.module.ts













let AppModule = class AppModule {
    configure(consumer) {
        consumer.apply(cors_namespaceObject.CorsMiddleware).forRoutes({ path: '*', method: common_namespaceObject.RequestMethod.ALL });
    }
};
AppModule = (0,external_tslib_namespaceObject.__decorate)([
    (0,common_namespaceObject.Module)({
        imports: [
            SuggestionsModule,
            ProductsModule,
            CategoriesModule,
            typeorm_namespaceObject.TypeOrmModule.forRoot({
                type: 'sqlite',
                database: 'christmas.db',
                entities: [ProductsEntity, CategoriesEntity, SuggestionsEntity],
                synchronize: true, //for database
            }),
        ],
        controllers: [AppController],
        providers: [AppService],
    })
], AppModule);


;// CONCATENATED MODULE: external "cors"
const external_cors_namespaceObject = require("cors");
;// CONCATENATED MODULE: ./src/main.ts
/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 * this file is responsible for bootstrapping the application
 */





// import * as socketio from 'socket.io';
// import * as http from 'http';//
async function bootstrap() {
    // An async function is defined with the keyword "async" before the function definition, and it always returns a promise, whether it is resolved or rejected
    const app = await core_namespaceObject.NestFactory.create(AppModule);
    const port = process.env['PORT'] || 3333;
    app.use(external_cors_namespaceObject());
    app.enableCors();
    // const server = http.createServer(app.getHttpServer());
    // const io = socketio(server);
    // io.origins('*:*');
    await app.listen(port); // The function will not continue the execution after await statement until the promise is resolved.
    common_namespaceObject.Logger.log(`🚀 Application is running on: http://localhost:${port}/`);
}
bootstrap();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map